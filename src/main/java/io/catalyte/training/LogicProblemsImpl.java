package io.catalyte.training;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * @author Will Boese
 * @version 1.0.0
 * @date 12/15/2022
 * This class contains method definitions for the Logic Problems
 */
public class LogicProblemsImpl implements LogicProblems {

  /**
   * gets the average of an array of scores, throws exception for negative scores
   * @param scores an array of test scores
   * @return the average of the scores to two decimal places
   */
  @Override
  public Double average(int[] scores) {

    double output = 0;

    if (scores.length == 0) {
      return output;
    }

    try {

      for (int score : scores) {
        if (score < 0) {
          throw new IllegalArgumentException("scores must be positive");
        }
        output += score;
      }

      output = output / scores.length;
      String roundedAverage = String.format("%.2f", output);
      output = Double.parseDouble(roundedAverage);

    } catch (IllegalArgumentException e) {
        System.out.print(e.getMessage());
    }
    return output;

  }

  /**
   * Gets the length of the last word in a string, throws exception for empty string
   * @param text any string
   * @return the length of the last word in the input string
   */
  @Override
  public int lastWordLength(String text) {

    int output = 0;

    try {

      if (Objects.equals(text, "")) {
        throw new IllegalArgumentException("input must not be an empty string");
      }

      text = text.trim();
      if (text.equals("")) {
        return 0;
      }

      int lastSpaceIndex = text.lastIndexOf(" ");
      String lastWord = text.substring(lastSpaceIndex + 1);

      output =  lastWord.length();

    } catch (IllegalArgumentException e) {
      System.out.print(e.getMessage());
    }

    return output;

  }

  /**
   * calculate all ladder paths by using a fibonacci sequence
   * @param rungs the number of rungs on a ladder
   * @return all possible ladder paths
   */
  @Override
  public BigDecimal distinctLadderPaths(int rungs) {

    ArrayList<BigDecimal> rungsArray = new ArrayList<>();
    BigDecimal output = new BigDecimal(0);

    try {
      if (rungs < 0) {
        throw new IllegalArgumentException("ladders can't have negative rungs");
      }

      for (int i = 0; i <= rungs; i++) {
        if (i < 4) {
          rungsArray.add(BigDecimal.valueOf(i));
        } else {
          rungsArray.add(rungsArray.get(i - 1).add(rungsArray.get(i - 2)));
        }
      }

      output = rungsArray.get(rungs);
    } catch (IllegalArgumentException e) {
      System.out.print(e.getMessage());
    }

    return output;
  }

  /**
   * Sorts an array of strings into a list of lists, throws exception for any empty strings
   * @param strs an array of strings
   * @return a list of lists, each list containing a grouping of strings with matching first and last letters
   */
  @Override
  public List<List<String>> groupStrings(String[] strs) {


    List<List<String>> output = new ArrayList<>(); // initialize the output variable(list to be returned)

    try {

      for (String str : strs) {
        if (str.trim().equals("")) {
          throw new IllegalArgumentException("strings must not be empty");
        }
      }

      if (strs.length == 0) {
        return output;
      }
      List<String> initialList = new ArrayList<>(); // first list to be nested
      initialList.add(strs[0]); // add first word in input array
      output.add(initialList); // add the initial nested list

      for ( int i = 1; i < strs.length; i++) { // outer loop

        boolean stringRequiresNewList = true;

        for (List<String> strings : output) { // inner loop

          String currentStr = strings.get(0);

          if (strs[i].charAt(0) == currentStr.charAt(0)
              && strs[i].charAt(strs[i].length() - 1) == currentStr.charAt(
              currentStr.length() - 1)) {
            strings.add(strs[i]); //add to appropriate nested list
            stringRequiresNewList = false;
            break;
          }
        } // end inner loop

        if (stringRequiresNewList) {
          List<String> newList = new ArrayList<>(); //creates new nested list
          newList.add(strs[i]);
          output.add(newList);
        }

      } // end outer loop

    } catch (IllegalArgumentException e) {
      System.out.print(e.getMessage());
    }

    return output;
  }

}
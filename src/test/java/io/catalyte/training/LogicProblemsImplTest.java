package io.catalyte.training;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *  * @author Will Boese
 *  * @version 1.0.0
 *  * @date 12/15/2022
 * This class runs all the tests for the LogicProblemsImpl class
 */
public class LogicProblemsImplTest {

  //TODO: Implement all requirements as specified in the requirements document

  private LogicProblemsImpl logicProblemsImpl;

  @BeforeEach
  public void init() {

    logicProblemsImpl = new LogicProblemsImpl();

  }

  @Test
  public void testAverageHappy1() {
    int[] testArray = {1,4,2};
    double result = logicProblemsImpl.average(testArray);
    double expected = 2.33;

    assertEquals(expected, result, "Wrong average. Expected: " + expected + ". Got: " + result);

  }

  @Test
  public void testAverageHappy2() {
    int[] testArray = {10};
    double result = logicProblemsImpl.average(testArray);
    double expected = 10.0;

    assertEquals(expected, result, "Wrong average. Expected: " + expected + ". Got: " + result);

  }

  @Test
  public void testAverageEmptyArray() {
    int[] testArray = {};
    double result = logicProblemsImpl.average(testArray);
    double expected = 0;

    assertEquals(expected, result, "Wrong average. Expected: " + expected + ". Got: " + result);

  }

  @Test
  public void testAverageNegativeException() {

    ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    PrintStream originalOut = System.out;
    PrintStream originalErr = System.err;
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));

    int[] testArray = {-1};
    String expected = "scores must be positive";
    logicProblemsImpl.average(testArray);
    assertEquals(
        expected,
        outContent.toString(),
        "Did not display expected values. Expected: " + expected + " Got: " + outContent);

    System.setOut(originalOut);
    System.setErr(originalErr);

  }

  @Test
  public void testLastWordLengthHappy() {

    String testString = "test this string";
    int result = logicProblemsImpl.lastWordLength(testString);
    int expected = 6;

    assertEquals(expected, result, "Wrong string length. Expected: " + expected + ". Got: " + result);

  }

  @Test
  public void testLastWordLengthSpaces() {

    String spacesString = "   ";
    int result = logicProblemsImpl.lastWordLength(spacesString);
    int expected = 0;

    assertEquals(expected, result, "Wrong string length. Expected: " + expected + ". Got: " + result);

  }

  @Test
  public void testWordLengthEmptyException() {

    ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    PrintStream originalOut = System.out;
    PrintStream originalErr = System.err;
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));

    String emptyString ="";
    String expected = "input must not be an empty string";
    logicProblemsImpl.lastWordLength(emptyString);
    assertEquals(
        expected,
        outContent.toString(),
        "Did not display expected values. Expected: " + expected + " Got: " + outContent);

    System.setOut(originalOut);
    System.setErr(originalErr);

  }

  @Test
  public void testDistinctLadderPaths100 (){
    int input = 100;
    BigDecimal expected = new BigDecimal("573147844013817084101");
    BigDecimal result = logicProblemsImpl.distinctLadderPaths(input);

    assertEquals(expected, result, "Wrong value returned. Expected: " + expected + ". Got: " + result);
  }

  @Test
  public void testDistinctLadderPaths0 (){
    int input = 0;
    BigDecimal expected = new BigDecimal("0");
    BigDecimal result = logicProblemsImpl.distinctLadderPaths(input);

    assertEquals(expected, result, "Wrong value returned. Expected: " + expected + ". Got: " + result);
  }

  @Test
  public void testDistinctLadderPathsNegativeException (){

    ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    PrintStream originalOut = System.out;
    PrintStream originalErr = System.err;
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));

    int input = -1;
    String expected = "ladders can't have negative rungs";
    logicProblemsImpl.distinctLadderPaths(input);
    assertEquals(
        expected,
        outContent.toString(),
        "Did not display expected values. Expected: " + expected + " Got: " + outContent);

    System.setOut(originalOut);
    System.setErr(originalErr);

  }

  @Test
  public void testGroupStringsHappy (){
    String[] input = {"arrange", "act", "assert", "ace"};
    List<List<String>> expected = new ArrayList<>();
    expected.add(Arrays.asList("arrange", "ace"));//{ {"arrange", "ace"}, {"act", "assert"}};
    expected.add(Arrays.asList("act", "assert"));
    List<List<String>> result = logicProblemsImpl.groupStrings(input);

    assertEquals(expected, result, "Wrong value returned. Expected: " + expected + ". Got: " + result);
  }

  @Test
  public void testGroupStringsEmptyArray (){
    String[] input = {};
    List<List<String>> expected = new ArrayList<>();
    List<List<String>> result = logicProblemsImpl.groupStrings(input);

    assertEquals(expected, result, "Wrong value returned. Expected: " + expected + ". Got: " + result);
  }

  @Test
  public void testGroupStringsEmptyStringException (){

    ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    PrintStream originalOut = System.out;
    PrintStream originalErr = System.err;
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));

    String[] input = {"arrange", "act", "assert", "ace", ""};
    String expected = "strings must not be empty";
    logicProblemsImpl.groupStrings(input);
    assertEquals(
        expected,
        outContent.toString(),
        "Did not display expected values. Expected: " + expected + " Got: " + outContent);

    System.setOut(originalOut);
    System.setErr(originalErr);

  }

}
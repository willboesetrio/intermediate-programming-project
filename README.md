# Intermediate Programming Project Template
CP-CO Module 2: Intermediate Programming Project
Author: Will Boese
Date: 12/15/2022

Description:
Intermediate Programming Project Template is used to give trainees a base project for demonstrating logical thinking. 
Testing:
In order to run tests, open the project in intelliJ and run the LogicProblemsImplTest.java file in the test directory.

## Installation

Fork the repository to your specified work directory, then clone it down

## Usage
Fill out LogicProblemsImpl with working solutions to the interface. Follow all requirements specified in the project instructions.

## Contributing
Feedback is welcome. If you see issues, or have ideas for improvement, please submit feedback!